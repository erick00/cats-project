import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CatsListComponent } from './components/cats-list/cats-list.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from './services/interceptor.service';
import { FavoritesCatsComponent } from './components/favorites-cats/favorites-cats.component';

@NgModule({
  declarations: [
    AppComponent,
    CatsListComponent,
    FavoritesCatsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    [{ provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true }]
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
