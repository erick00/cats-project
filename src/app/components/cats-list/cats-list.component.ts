import { Component, OnInit } from '@angular/core';
import { CatsService } from 'src/app/services/cats.service';

@Component({
  selector: 'app-cats-list',
  templateUrl: './cats-list.component.html',
  styleUrls: ['./cats-list.component.scss']
})
export class CatsListComponent implements OnInit {
  catsList: any = [];
  constructor( private catsService: CatsService ) { }

  ngOnInit() {
    this.getCatsList();
  }

  addToFavorites(id:number) {
    this.catsService.addCatToFavouriteList(id).subscribe((res) => {
      const idx = this.catsList.findIndex((item) => item.id === id )
      this.catsList.splice(idx, 1);
    })
  }

  getCatsList() {
    this.catsService.getCatsList(10).subscribe((res) => {
      this.catsList = res;
    })
  }

}
