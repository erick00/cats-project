import { Component, OnInit } from '@angular/core';
import { CatsService } from 'src/app/services/cats.service';

@Component({
  selector: 'app-favorites-cats',
  templateUrl: './favorites-cats.component.html',
  styleUrls: ['./favorites-cats.component.scss']
})
export class FavoritesCatsComponent implements OnInit {
  catsList: any = [];
  constructor( private catsService: CatsService ) { }

  ngOnInit() {
    this.getFavoriteCatsList();
  }

  removeFromFavorites(catId) {
    this.catsService.deleteCatFromFavoriteList(catId).subscribe((res) => {
      const idx = this.catsList.findIndex((item) => item.id === catId )
      this.catsList.splice(idx, 1);
    })
  }


  getFavoriteCatsList() {
    this.catsService.getFavouriteCatsList(10).subscribe((res) => {
      this.catsList = res
    })
  }
}
