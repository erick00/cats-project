import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoritesCatsComponent } from './favorites-cats.component';

describe('FavoritesCatsComponent', () => {
  let component: FavoritesCatsComponent;
  let fixture: ComponentFixture<FavoritesCatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoritesCatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoritesCatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
