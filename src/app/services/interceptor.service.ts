import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const request = req;
    request = req.clone({
      setHeaders: {
        'x-api-key': '0deb10af-017e-4cb1-b4a8-c3d08d691587',
        'Content-Type': 'application/json',
      }
    })

    return next.handle(request)
  }
}
