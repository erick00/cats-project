import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
// environment

@Injectable({
  providedIn: 'root'
})
export class CatsService {

  constructor( private _httpClient: HttpClient) { }

  getCatsList(limit: number) {
    return this._httpClient.get(`${environment.API_URI}/images/search?limit=${limit}`);
  }

  getFavouriteCatsList(limit: number) {
    return this._httpClient.get(`${environment.API_URI}/favourites?limit=${limit}`);
  }

  addCatToFavouriteList(imageId: number) {
    return this._httpClient.post(`${environment.API_URI}/favourites`, {'image_id': imageId});
  }
  
  deleteCatFromFavoriteList(catId: number) {
    return this._httpClient.delete(`${environment.API_URI}/favourites/${catId}`);
  }
}
