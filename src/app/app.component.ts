import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dacodes-prueba';

  constructor(private router: Router) {}

  gotTo(path) {
    this.router.navigate([path])
  }
}
