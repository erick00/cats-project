import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatsListComponent } from './components/cats-list/cats-list.component';
import { FavoritesCatsComponent } from './components/favorites-cats/favorites-cats.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  }, {
    path: 'main',
    component: CatsListComponent
  }, {
    path: 'favorites',
    component: FavoritesCatsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
